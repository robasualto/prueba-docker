Utilizando el Dockerfile anterior, elabore una forma de despliegue para actualizar el documento “index.html” sin necesidad de recrear el contenedor cada vez que este sea modificado. Adjunte su solución en el archivo README.md correspondiente.

1. Para esta pregunta se uso el Dockerfile anterior sin hacer ninguna modificacion, la resolución fue agregar un volumen "-v" para no tener que recrear el contenedor cada vez que se hicieran cambios en la carpeta Zippylab y compartir el directorio completo de ~/Zippylab/ con /usr/share/nginx/html/

`$ docker run -v ~/Zippylab/:/usr/share/nginx/html/ -p 8080:80 -d html-zippy:v1`
