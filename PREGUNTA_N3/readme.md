Despliegue un contenedor con MySQL, luego cree un usuario y contraseña, asegúrese de que tiene conectividad desde su equipo local a la instancia de BD. Adjunte el comando para crear este contenedor.

Para aprovechar el conocimiento aprendido de la pregunta anterior le agregue persistencia de datos mediante volumenes a este desarrollo y use docker-compose

1.Primero creamos nuesta carpeta

`$ mkdir mysql`

2.Accedemos a nuestra carpeta

`$ cd mysql/`

3.Creamos nuestro Dockerfile

`$ touch Dockerfile`

4.Editamos nuestro Dockerfile y le agregamos la imagen a utilizar

`$ vim Dockerfile`

5.Creamos nuestro archivo docker-compose.yaml

`$ touch docker-compose.yaml`

6.Editamos nuestro docker-compose.yaml

`$ vim docker-compose.yaml`

7.Una vez editado nuestro archivo .yaml levantamos nuestro contenedor 

`$ docker-compose up -d`

8. Para validar la conexión de nuestra base de datos con su nuevo usuario usaremos dbeaver

`$ sudo apt update`

`$ sudo snap install dbeaver-ce`

9. Una vez instalado procedemos a conectarnos a nuestra base de datos. hacemos la configuración de use SSL=false y AllowPublicKeyRetrieval=True. Validamos que nuestra conexion esta hecha correctamente 

https://prnt.sc/1ztf9jf

